
Internet Explorer module provides a simple way to notify users that their version of Internet Explorer is unsupported. The version(s) of IE are easily configured, along with the message itself, display options, and JavaScript popup capabilities.

Internet Explorer module is the work of Mike Crittenden (http://mikethecoder.com).


Dependencies
------------
None


Install
-------
1. Download and unpackage the module. 
2. Copy the "ie" folder to your modules directory (usually /sites/all/modules)
3. Enable the module in the admin interface
4. Go to Site configuration -> Internet Explorer and configure the options.
5. Enable the newly created "IE Notification" Block from the Blocks admin page.